#!/bin/bash
# setup error handling
trap 'echo "Error in line $LINENO" >&2 ; exit 1' ERR
set -E -o pipefail

# ---- configuration variables ----
spack_git='https://github.com/mpsd-computational-science/spack.git'
spack_git_ssh='git@github.com:mpsd-computational-science/spack.git'
# forked from spack release v0.19.2
spack_branch='mpsd/v0.19_23b'
system_compiler="gcc"
generic_arch="x86_64_v2"
# default: spack cache is one level above pwd
# setup the build cache directory
# This could be a symlink to  different location ( e.g from a different run of the script)
# and hence we need to resolve the path to the real location
mkdir -p ../mpsd-spack-cache # create cache directory if it does not exist
spack_cache=$(realpath  ../mpsd-spack-cache)
lmod_root=`pwd`/lmod
buildcache_upload=1 # set to 0 to skip build cache creation
buildcache_signing_key=''

unset DISPLAY
# override user locale to make output more easily parsable
if locale -a | grep -q '^C\.UTF-8' ; then
    export LC_ALL='C.UTF-8'
    export LANG='C.UTF-8'
else
    export LC_ALL='C'
    export LANG='C'
fi
export SPACK_DISABLE_LOCAL_CONFIG=true
export SPACK_USER_CACHE_PATH=$spack_cache

use_color=0
color_spack='spack'
color_git='git'
C_MSG=`echo -ne '\033[0;35m'`
C_ERR=`echo -ne '\033[0;31;1m'`
C_RESET=`echo -ne '\033[0m'`

debug=0

# ---- Detect location of this script, so we can find config files later on ----
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )

# ---- Function definitions ----
#   -- utility functions for rough profiling
# usage:
#   start_time=$(time_elapsed 0)
#   ...
#   time_consumed=$(time_elapsed $start_time)
#   echo "Time consumed: $(time_format $time_consumed)"
# reasoning:
#   - bash only supports integer arithmetics
#   - when going for higher time resolution, timestamps like
#     $start_time may exceed the integer range
#   - but time ranges like $time_consumed should be ok
#     (but need to be calculated and converted to integers externally)
#   - accumulation of time ranges can use bash's integer arithmetic operations
profile_precision_millisecond=1
if [ $profile_precision_millisecond -lt 1 ]; then
    time_elapsed() {
        local start_time=${1}
        echo $((SECONDS - start_time))
    }

    time_format() {
        local T=${1}
        printf "%02d:%02d:%02d" $((T/60/60)) $((T/60%60)) $((T%60))
    }
else
    # macOs 'date' does not support "%N".
    # simpler and more portable:
    # use perl for high-resultion time differences (adds <10ms, mostly compensated by the subtraction)
    time_elapsed() {
        perl -mTime::HiRes -e 'printf "%.0f\n", (Time::HiRes::time() * 1000 - $ARGV[0]);' $1
    }

    time_format() {
        local T=$((${1} / 1000))
        printf "%02d:%02d:%02d" $((T/60/60)) $((T/60%60)) $((T%60))
    }
fi

die() {
    local msg=${1}
    local exitcode=${2}
    [ -n "$exitcode" ] || exitcode=1
    echo "${C_ERR}############${C_RESET} $msg ${C_ERR}############${C_RESET}" >&2
    exit $exitcode
}

setup_spack_time=0
setup_spack() {
    local func_start_time=$(time_elapsed 0)

    # ---- now setup/update the spack install ----
    echo "${C_MSG}#####${C_RESET} Setting up spack"
    if [ ! -d spack ] ; then
        $color_git clone --branch $spack_branch $spack_git
    else
        cd spack
        $color_git fetch
        $color_git checkout $spack_branch
        $color_git pull --rebase
        cd ..
    fi
    # override spack defaults with config from mpsd-spack-environments
    rsync -a  --checksum --out-format="Updating spack/%n%L" $script_dir/spack_overlay/ spack/
    # setup gpg keys for binary mirror
    mkdir -p spack/opt/spack/
    if [ $buildcache_upload -eq 1 ]; then
        if [ -d $HOME/.gnupg ] ; then
            ln -nsf $HOME/.gnupg spack/opt/spack/gpg
        else
            die "missing gpg config in $HOME/.gnupg (necessary for spack cache updates and verification)"
        fi
    fi

    # activate the spack instance
    . spack/share/spack/setup-env.sh

    # ---- create spack cache/mirror directory if necessary ----
    if [ ! -d $spack_cache/build_cache ]; then
        mkdir -p $spack_cache/build_cache
    elif [ $buildcache_upload -ne 0 ] ; then
        # rebuild buildcache index in case we were interrupted before
        $color_spack buildcache update-index -d $spack_cache
    fi

    # setup mirror/buildcache
    if ! spack mirror list | grep -q 'mpsd-spack-cache' ; then
        $color_spack mirror add --scope site mpsd-spack-cache $spack_cache
    fi
    echo "${C_MSG}#####${C_RESET} find base compiler and libraries"
    $color_spack compiler find --scope site
    # $color_spack external find --scope site --all
    # $color_spack external find --scope site --not-buildable ncurses readline
    setup_spack_time=$((setup_spack_time + $(time_elapsed $func_start_time)))
}

update_buildcache_time=0
update_buildcache() {
    local func_start_time=$(time_elapsed 0)
    local old_tmpdir=$TMPDIR
    export TMPDIR=`mktemp --tmpdir -d spack-environments.XXXXXXXXXX`
    echo "${C_MSG}#####${C_RESET} update spack binary/build cache with new packages"
    spack buildcache list -aL | perl -ne '/^(\S{32})\s+(\S+)/ and print "$2/$1\n";' | sort >$TMPDIR/buildcache.list
    spack find --no-groups --format '{name}@{version}/{hash}' | sort >$TMPDIR/spack-find.list
    comm -13 $TMPDIR/buildcache.list $TMPDIR/spack-find.list >$TMPDIR/buildcache-upload.list
    if [ -s $TMPDIR/buildcache-upload.list ] ; then
        perl -ne 'print "    $_";' $TMPDIR/buildcache-upload.list
        # if buildcache_upload is not set, skip buildcache update and message that this was requested by user
        if [ $buildcache_upload -ne 0 ] ; then
            $color_spack buildcache create -a --only package -d $spack_cache ${buildcache_signing_key:+-k $buildcache_signing_key} $(cat $TMPDIR/buildcache-upload.list)
            $color_spack buildcache update-index -d $spack_cache
        else
            echo "${C_MSG}#####${C_RESET} skipping buildcache upload by user request"
        fi
    else
        echo "    ${C_MSG}no new packages found${C_RESET}"
        if [ "$debug" -gt 0 ]; then
            echo "DEBUG: ------- comm begin ------"
            comm $TMPDIR/buildcache.list $TMPDIR/spack-find.list | perl -ne 'print "DEBUG: $_"'
            echo "DEBUG: ------- comm end ------"
        fi
    fi
    rm -rf $TMPDIR
    if [ -z "$old_tmpdir" ] ; then
        unset TMPDIR
    else
        export TMPDIR=$old_tmpdir
    fi
    update_buildcache_time=$((update_buildcache_time + $(time_elapsed $func_start_time)))
}

install_missing_terminfo() {
    local ncurses_specs=$(spack find --no-groups --format '{name}@{version}/{hash}' ncurses) || return 0
    [[ "$ncurses_specs" =~ ^[[:alpha:]] ]] || return 0
    echo "${C_MSG}#####${C_RESET} installing missing ncurses terminfo entries for '$ncurses_specs'"
    local ncurses_spec
    for ncurses_spec in $ncurses_specs; do
        local terminfo_dir="$(spack location -i $ncurses_spec)/share/terminfo/"
        rsync -a --out-format="Updating ${terminfo_dir}%n%L" --include='*/' --prune-empty-dirs \
            --include /r/rxvt-unicode --include /r/rxvt-unicode-256color \
            --exclude '*' /lib/terminfo/ ${terminfo_dir}
    done
}


postprocess_spack_time=0
postprocess_spack() {
    local func_start_time=$(time_elapsed 0)
    install_missing_terminfo
    update_buildcache
    echo "${C_MSG}#####${C_RESET} (Re-)Creating module files"
    $color_spack module lmod refresh -y
    postprocess_spack_time=$((postprocess_spack_time + $(time_elapsed $func_start_time)))
}

create_toolchain_module() {
    local toolchain_name=${1}
    local toolchain_compiler=${2}
    echo "${C_MSG}#####${C_RESET} Creating toolchain module '$toolchain_name'"
    $color_spack env activate $toolchain_name
    $color_spack env loads -m lmod >/dev/null
    $color_spack env deactivate
    loads="$SPACK_ROOT/var/spack/environments/$toolchain_name/loads"
    lmod_toolchain_root="$lmod_root/Core/toolchains/"
    mkdir -p $lmod_toolchain_root
    env_file="$lmod_toolchain_root/$toolchain_name.lua"
    echo '-- -*- lua -*-' >$env_file
    echo "depends_on(\"${toolchain_compiler/@/\/}\")" >>$env_file
    perl -ne '/^module load (.+)$/ and print "depends_on(\"$1\")\n";' $loads >>$env_file
    # add the appropriate configure wrapper into the MPSD_OCTOPUS_CONFIGURE environment variable
    echo "setenv(\"MPSD_OCTOPUS_CONFIGURE\", \"$script_dir/octopus/${toolchain_name}-config.sh\")" >>$env_file
}

install_toolchain_time=0
install_toolchain() {
    local func_start_time=$(time_elapsed 0)
    local toolchain_name=${1}
    local toolchain_config="$script_dir/toolchains/$toolchain_name"
    [ -d $toolchain_config ] || die "ERROR: missing config dir $toolchain_config"

    local toolchain_compiler
    [ ! -s $toolchain_config/compiler_vars.sh ] || . $toolchain_config/compiler_vars.sh

    if [ -n "$toolchain_compiler" ] ; then
        if spack compiler info "${toolchain_compiler}%${system_compiler}" >/dev/null 2>&1; then
            echo "${C_MSG}#####${C_RESET} re-using installed toolchain compiler '$toolchain_compiler'"
        else
            echo "${C_MSG}#####${C_RESET} install toolchain compiler '$toolchain_compiler'"
            $color_spack install "${toolchain_compiler}%${system_compiler}" arch="${generic_arch}"
            $color_spack load "${toolchain_compiler}%${system_compiler}"
            $color_spack compiler find --scope site
            $color_spack unload "${toolchain_compiler}%${system_compiler}"
            postprocess_spack
        fi
    else
        echo "${C_MSG}#####${C_RESET} no toolchain compiler specified, falling back to system compiler '$system_compiler'"
        toolchain_compiler=$system_compiler
    fi

    install_global_packages $toolchain_name

    if [ -s "$toolchain_config/spack.yaml" ] ; then
        if [[ " $(spack env list | tr '\n' ' ') " =~ " $toolchain_name " ]]; then
            echo "${C_MSG}#####${C_RESET} Environment $toolchain_name does already exist; updating config" >&2
            rsync --checksum --out-format="Updating spack/var/spack/environments/$toolchain_name/%n%L" "$toolchain_config/spack.yaml" "$SPACK_ROOT/var/spack/environments/$toolchain_name/spack.yaml"
        else
            echo "${C_MSG}#####${C_RESET} Creating environment '$toolchain_name'"
            $color_spack env create "$toolchain_name" "$toolchain_config/spack.yaml"
        fi

        $color_spack env activate "$toolchain_name"
        local env_install_successful=1
        $color_spack install || env_install_successful=0
        $color_spack compiler find --scope site
        $color_spack env deactivate
        postprocess_spack
        [ $env_install_successful -eq 1 ] || die "Environment creation failed"

        create_toolchain_module $toolchain_name $toolchain_compiler
    else
        echo "${C_MSG}#####${C_RESET} Not creating spack environment for toolchain '$toolchain_name': missing $toolchain_config/spack.yaml"
    fi
    install_toolchain_time=$((install_toolchain_time + $(time_elapsed $func_start_time)))
}

install_global_packages() {
    local toolchain_name=${1}
    # set package_arch to generic arch if  toolchain_name has `_generic suffix else use `spack arch`
    local package_arch="${generic_arch}"
    [[ "$toolchain_name" =~ _generic$ ]] || package_arch="$(spack arch)"

    local global_package_list="$script_dir/toolchains/$toolchain_name/global_packages.list"
    [ -s $global_package_list ] || return 0
    echo "${C_MSG}#####${C_RESET} Installing global packages from $global_package_list"
    local package
    while read package; do
        # strip comments
        package=${package%#*}
        # remove leading whitespace characters
        package="${package#"${package%%[![:space:]]*}"}"
        # remove trailing whitespace characters
        package="${package%"${package##*[![:space:]]}"}"
        [ -n "$package" ] || continue
        [[ "$package" =~ '%' ]] || package="${package}%${system_compiler}"
        spack find ${package} arch="${package_arch}" >/dev/null 2>&1 && continue
        echo "${C_MSG}#####${C_RESET}     spack install ${package} arch='${package_arch}'"
        $color_spack install ${package} arch="${package_arch}"
    done <$global_package_list
    postprocess_spack
}

print_help() {
    local exitcode=${1}
    [ -n "$exitcode" ] || exitcode=0
    cat >&2 <<EOF

    Usage: spack_setup.sh [-hscdb] [-k <key-id>] [--help] <TOOLCHAIN> [TOOLCHAIN ...]

    OPTIONS:
      --help, -h  Print help message
      -s          Use SSH instead of HTTPS when cloning spack git
      -c          Always use colors in output
      -d          increase debug level in log (may be specified multiple times)
      -b          Skip buildcache creation/updload
      -k <key-id> use gpg <key-id> to sign uploads to buildcache

    TOOLCHAIN argument possible values:
EOF
    ls $script_dir/toolchains/ | sed -e 's/^/      /'
    echo
    exit $exitcode
}

# --- MAIN, execution starts here ----

script_start_time=$(time_elapsed 0)
[ -t 1 ] && use_color=1
# rudimentary parsing of long options with bash's getopts is inspired by
#   https://stackoverflow.com/a/7680682
while getopts ":hscdbk:-:" OPTNAME; do
    case "$OPTNAME" in
        -)
            case ${OPTARG} in
                help)
                    print_help 0
                    ;;
                *)
                    die "ERROR: unknown option --${OPTARG}"
                    ;;
            esac
            ;;
        h)
            print_help 0
            ;;
        s)
            spack_git=$spack_git_ssh
            ;;
        c)
            use_color=1
            ;;
        d)
            debug=$((debug + 1))
            ;;
        :)
            die "ERROR: option -${OPTARG} requires an argument."
            ;;
        b)
            buildcache_upload=0
            ;;
        k)
            buildcache_signing_key=${OPTARG}
            ;;
        *)
            die "ERROR parsing options: unknown option -${OPTARG}"
            ;;
    esac
done
shift $((OPTIND-1))

[ $# -gt 0 ] || print_help 1
if [ $use_color -gt 0 ] ; then
    color_spack="spack --color=always"
    color_git="git -c color.ui=always"
else
    C_MSG=''
    C_ERR=''
    C_RESET=''
fi

[ -n "$BASH_VERSINFO" ] || die "Requires bash >=4 (no \$BASH_VERSIONFO found)"
[ ${BASH_VERSINFO[0]} -gt 4 ] || die "Requires bash >=4 (\$BASH_VERSINFO: ${BASH_VERSINFO[*]})"
type spack >/dev/null 2>&1 && die "A spack instance seems to be loaded already, please rerun without loading spack/setup-env before"

setup_spack

for toolchain_arg ; do
    install_toolchain $toolchain_arg
done

echo "${C_MSG}#####${C_RESET} Installation finished in $(time_format $(time_elapsed $script_start_time)) (\
 setup: $(time_format $((setup_spack_time))),\
 buildcache: $(time_format $((update_buildcache_time))),\
 install: $(time_format $((install_toolchain_time - postprocess_spack_time))),\
 lmod: $(time_format $((postprocess_spack_time - update_buildcache_time)))\
)"
echo "${C_MSG}#####${C_RESET} 'module use $lmod_root/Core' can be used to get the new environments (note that lmod does **not** allow trailing '/')"
