# make targets for making and runing the spack-environments
# image  - make ocotpus using "toolchain=foss2021a-mpi"
# image-serial - make octopus using "toolchain=foss2021a-serial"
# no-cache variants do not use docker cache
# run - run the latest container
# run-mount  -  mounts a folder ../tmps to /tmps in docker ( make sure it exists in host before running)
#
# The docker containers are tagged with the git branch name

gitBranch=`git rev-parse --abbrev-ref HEAD | sed 's/[^a-zA-Z0-9]/-/g'`
image : image-mpi

# Mainstream images
image-mpi: image-foss2021a-mpi 
image-serial: image-foss2022a-serial
image-global-packages: image-foss2021a-global-packages
image-cuda-mpi: image-foss2021a-cuda-mpi


# All possible toolchains
image-foss2021a-serial:
	docker build --build-arg "toolchain=foss2021a-serial" -f Dockerfile -t spack-environments-serial:${gitBranch} .

image-foss2021a-mpi:
	docker build --build-arg "toolchain=foss2021a-mpi" -f Dockerfile -t spack-environments-mpi:${gitBranch} .

image-foss2021a-cuda-mpi:
	docker build --build-arg "toolchain=foss2021a-cuda-mpi" -f Dockerfile -t spack-environments-cuda-mpi:${gitBranch} .

image-foss2021a-global-packages :
	docker build --build-arg "toolchain=global" -f Dockerfile -t spack-environments-global:${gitBranch} .

image-foss2022a-serial:
	docker build --build-arg "toolchain=foss2022a-serial" -f Dockerfile -t spack-environments-serial:${gitBranch} .

image-foss2022a-mpi:
	docker build --build-arg "toolchain=foss2022a-mpi" -f Dockerfile -t spack-environments-mpi:${gitBranch} .
	
image-no-cache :
	docker build --no-cache -f Dockerfile -t spack-environments:${gitBranch} .

image-serial-no-cache :
	docker build --no-cache --build-arg "toolchain=foss2021a-serial" -f Dockerfile -t spack-environments-serial:${gitBranch} .


run: run-mpi

run-mpi:
	docker run -ti -v $PWD:/io  spack-environments-mpi:${gitBranch} env TERM=xterm-256color bash -l

run-serial:
	docker run -ti -v $PWD:/io  spack-environments-serial:${gitBranch} env TERM=xterm-256color bash -l

run-global:
	docker run -ti -v $PWD:/io  spack-environments-global:${gitBranch} env TERM=xterm-256color bash -l