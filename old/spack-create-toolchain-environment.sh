# /usr/bin/bash

# Do this in bash? Python? Just? Emerge?
#
# Assume
# - spack is in path.
# - the right gcc is available
# - no environment is active
# - the $toolchain environment does not exist yet

toolchain=$1

if [ $# -ne 1 ];  then
    echo "Please pass the name of the toolchain as an argument. Exitting."
else
    echo "About to install toolchain $1..."
fi

# TODO check that no environment is active

# TODO check $toolchain environment does not exist yet

spack env create $toolchain $toolchain/spack.yaml

spack env activate -p $toolchain

spack install


#

echo "If you need to tidy up and start again, these may help:"
echo "    $ despacktivate"
echo "    $ spack env remove $toolchain"



