# /usr/bin/bash

set -e

# Do this in bash? Python? Just? Emerge?
#
# Assume
# - spack is in path.
# - the right gcc is available
# - no environment is active
# - the $toolchain environment does not exist yet

# need built script later, expect at BASE/octopus/run_config.sh

BASE=$(pwd)

toolchain=$1

if [ $# -ne 1 ];  then
    echo "Please pass the name of the toolchain as an argument. Exitting."
    exit 1
else
    echo "About to compile octopus with toolchain $1..."
fi

# TODO check that no environment is active

spack load gcc@10.3.0
spack env activate -p $toolchain

# get octopus
TMP=$(mktemp -d)
pushd $TMP
git clone https://gitlab.com/octopus-code/octopus.git


cd octopus && autoreconf -i
mkdir _build
cd _build
bash $BASE/octopus/run_config.sh
make -j 16

echo "Build octopus at $TMP"
