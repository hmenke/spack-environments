# Upgrade of HPC system

See also [vision](vision.org) and [Readme](Readme.org).

Notes below from meeting 24 Februar 2022, with Henning, Heiko, Martin, Sascha, Hans

Original pad at: https://pad.gwdg.de/iK9S9SXYRW2PmT-BT3NqjQ?edit

## desired software

- matlab
- mathematica
- intel compiler 
- further modules
  - via spack

- for compiler in [gnu compiler, intel]:
  - fftw, gsl, blas, lapack

## existing modules

- okay to lose
- create new ones as complaints come in

## octopus build bot
- ignore for now

## potential users
- Franco
- Kevin Lively
- Michael Fechner
- Tamme Wollweber
- Kartik

## Module hierarchy

- choice of compiler
- choice of MPI

## cluster infrastructure
- slurm scheduler
- new master on virtual machine
- GPU support

## Questions

- support for different architectures

# Order of steps

## hardware focus
- new replacement slurm master on virtual machine
- upgrade idataplex to bullseye, and make it a client to the slurm master
- repeat with GPU node
  - check slurm GPU reservation 
- repeat with bigmem node
  - check disk controller

- sort out software (see below)
- check partition tables are appropriate for spack distribution of software
- upgrade all HPC nodes to Bullseye (official down time)
- support users to make everything work...

- upgrade /home to new ZFS version from Bullseye
  - create one dataset per user

## software focus
- install intel compiler as debian package
  - update license server in Goettingen (required?)
- create minimal set of compilers and libraries through spack
  - gcc / intel
- investigate support for different architectures
  - automatically load the right version of the module
- go through user list and check requirements can be fulfilled on upgrade systems

### module interface
- hierarchical to avoid too long lists of modules

## feedback Martin - current issues

- octopus compilation is difficult
  - uses system libraries where it should be using spack libraries

- not tried intel compiler yet
    
# Octopus
- offer latest release (octopus/11.4)
- nightly build (octopus/develop)
  - either fetch from buildbots
  - or recompile for each architecture

