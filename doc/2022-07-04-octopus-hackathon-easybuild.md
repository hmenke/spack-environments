

https://gitlab.com/octopus-code/buildbot/easyconfigs/-/blob/master/o/Octopus/Octopus-devel-foss-2021a-serial.eb
```python
easyblock = 'BuildEnv'

name = 'Octopus'
version = 'devel'
versionsuffix = '-serial'

homepage = 'None'
description = """This module sets a group of environment variables for compilers, linkers, maths libraries, etc., required
 to compile Octopus."""

toolchain = {'name': 'foss', 'version': '2021a'}

dependencies = [
    ('Autotools', '20210128'),
    ('libxc', '4.3.4'),
    ('FFTW', '3.3.9'),
    ('ETSF_IO', '1.0.4'),
    ('GSL', '2.7'),
    ('SPARSKIT2', '2021.06.01'),
    ('NLopt', '2.7.0'),
    ('libgd', '2.3.1'),
    ('libpspio', '0.2.4'),
    ('libvdwxc', '0.4.0', '-serial'),
    ('NFFT', '3.2.4'),
    ('futile', '1.8.3', '-serial'),
    ('PSolver', '1.8.3', '-serial'),
    ('BerkeleyGW', '1.2.0', '-serial'),
    ('Python', '3.9.5'),
    ('CGAL', '5.2'),
    ('DFTB+', '21.1', '-serial'),
]


moduleclass = 'devel'
```

https://gitlab.com/octopus-code/buildbot/easyconfigs/-/blob/master/o/Octopus/Octopus-devel-foss-2021a.eb
```python
easyblock = 'BuildEnv'

name = 'Octopus'
version = 'devel'

homepage = 'None'
description = """This module sets a group of environment variables for compilers, linkers, maths libraries, etc., required
 to compile Octopus."""

toolchain = {'name': 'foss', 'version': '2021a'}

dependencies = [
    ('Autotools', '20210128'),
    ('libxc', '4.3.4'),
    ('FFTW', '3.3.9'),
    ('ETSF_IO', '1.0.4'),
    ('GSL', '2.7'),
    ('SPARSKIT2', '2021.06.01'),
    ('ELPA', '2020.05.001'),
    ('NLopt', '2.7.0'),
    ('libgd', '2.3.1'),
    ('libpspio', '0.2.4'),
    ('libvdwxc', '0.4.0'),
    ('NFFT', '3.2.4'),
    ('futile', '1.8.3'),
    ('PSolver', '1.8.3'),
    ('BerkeleyGW', '1.2.0'),
    ('Python', '3.9.5'),
    ('ParMETIS', '4.0.3'),
    ('PFFT', '1.0.8-alpha'),
    ('PNFFT', '1.0.7-alpha'),
    ('CGAL', '5.2'),
    ('DFTB+', '21.1'),
]


moduleclass = 'devel'
```

https://gitlab.com/octopus-code/buildbot/easyconfigs/-/blob/master/o/Octopus/Octopus-devel-intel-2021a-serial.eb
```python
easyblock = 'BuildEnv'

name = 'Octopus'
version = 'devel'
versionsuffix = '-serial'

homepage = 'None'
description = """This module sets a group of environment variables for compilers, linkers, maths libraries, etc., required
 to compile Octopus."""

toolchain = {'name': 'intel', 'version': '2021a'}

dependencies = [
    ('Autotools', '20210128'),
    ('libxc', '4.3.4'),
    ('ETSF_IO', '1.0.4'),
    ('GSL', '2.7'),
    ('SPARSKIT2', '2021.06.01'),
    ('ELPA', '2021.05.001'),
    ('NLopt', '2.7.0'),
    ('libgd', '2.3.1'),
    ('libpspio', '0.2.4'),
    ('libvdwxc', '0.4.0', '-serial'),
    ('NFFT', '3.2.4'),
    ('futile', '1.8.3', '-serial'),
    ('PSolver', '1.8.3', '-serial'),
    ('BerkeleyGW', '1.2.0', '-serial'),
    ('Python', '3.9.5'),
    ('CGAL', '5.2'),
    ('DFTB+', '21.1', '-serial'),
]


moduleclass = 'devel'
```

https://gitlab.com/octopus-code/buildbot/easyconfigs/-/blob/master/o/Octopus/Octopus-devel-intel-2021a.eb
```python
easyblock = 'BuildEnv'

name = 'Octopus'
version = 'devel'

homepage = 'None'
description = """This module sets a group of environment variables for compilers, linkers, maths libraries, etc., required
 to compile Octopus."""

toolchain = {'name': 'intel', 'version': '2021a'}

dependencies = [
    ('Autotools', '20210128'),
    ('libxc', '4.3.4'),
    ('FFTW', '3.3.9'),
    ('ETSF_IO', '1.0.4'),
    ('GSL', '2.7'),
    ('SPARSKIT2', '2021.06.01'),
    ('ELPA', '2020.05.001'),
    ('NLopt', '2.7.0'),
    ('libgd', '2.3.1'),
    ('libpspio', '0.2.4'),
    ('libvdwxc', '0.4.0'),
    ('NFFT', '3.2.4'),
    ('futile', '1.8.3'),
    ('PSolver', '1.8.3'),
    ('BerkeleyGW', '1.2.0'),
    ('Python', '3.9.5'),
    ('ParMETIS', '4.0.3'),
    ('PFFT', '1.0.8-alpha'),
    ('PNFFT', '1.0.7-alpha'),
    ('CGAL', '5.2'),
    ('DFTB+', '21.1'),
]


moduleclass = 'devel'
```
