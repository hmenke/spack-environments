FROM debian:bullseye

ENV MPSD_OS=linux-debian11
ARG toolchain=foss2021a-mpi # or foss2021a-serial
ENV TERM=xterm-256color





# Get basic packages to setup mpsd mirror
RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    gnupg \
    wget \
    && rm -rf /var/lib/apt/lists/*


#Set the MPSD mirrors
COPY docker /root/docker
RUN echo "deb http://mpsd-deb-bullseye.desy.de/debian/ bullseye main non-free contrib" > /etc/apt/sources.list && \
    echo "deb http://mpsd-deb-bullseye.desy.de/debian-security/ bullseye-security main contrib non-free" >> /etc/apt/sources.list && \
    echo "deb http://mpsd-deb-bullseye.desy.de/debian/ bullseye-updates main contrib non-free" >> /etc/apt/sources.list && \
    echo "deb http://mpsd-deb-bullseye.desy.de/debian/ bullseye-backports main contrib non-free" >> /etc/apt/sources.list && \
    echo "deb http://mpsd-deb-bullseye.desy.de/debian-mpsd/ mpsd-bullseye/" >> /etc/apt/sources.list && \
    echo "deb http://mpsd-deb-bullseye.desy.de/debian-mpsd/ mpsd-bullseye-nonfree/" >> /etc/apt/sources.list && \
    echo "deb http://mpsd-deb-bullseye.desy.de/debian-mpsd/ mpsd-bullseye-3rdparty/" >> /etc/apt/sources.list && \
    echo "deb http://mpsd-deb-bullseye.desy.de/debian-mpsd/ mpsd-bullseye-backports/" >> /etc/apt/sources.list && \
    echo "deb http://mpsd-deb-bullseye.desy.de/debian-mpsd/ mpsd-bullseye-easybuild/" >> /etc/apt/sources.list && \
    apt-key add /root/docker/MPSD.asc && \
    apt-get -y update

# Convenience tools, if desired for debugging etc
# RUN apt-get -y install wget time nano vim emacs git

# From https://github.com/ax3l/dockerfiles/blob/master/spack/base/Dockerfile:
# install minimal spack dependencies
RUN apt-get install -y --no-install-recommends \
    autoconf \
    build-essential \
    coreutils \
    file \
    gfortran \
    git \
    openssh-server \
    python-is-python3 \
    unzip \
    vim \
    nano \
    rsync \
    sudo \
    lmod \
    automake \
    libtool \
    linux-headers-$(uname -r) \
    && rm -rf /var/lib/apt/lists/*






# Run the commands to setup spack and install the toolchain
RUN adduser user
USER user
## move the spack-environments repository in to the docker
RUN mkdir -p /home/user/spack-environments/
COPY . /home/user/spack-environments/
WORKDIR /home/user/spack-environments/

## setup the toolchains
# RUN ssh-keygen -b 2048 -t rsa -f /home/user/.ssh/id_rsa -q -N "" && \
#     eval `ssh-agent -s` && \
#     ssh-add ~/.ssh/id_rsa
RUN gpg --import /home/user/spack-environments/docker/MPSD.asc
RUN /bin/bash /home/user/spack-environments/spack_setup.sh -b ${toolchain}


# If testing toolchains, load the freshly made toolchain and compile octopus and run checks
# If the toolchain variable is 'global' then exit(0) and do nothing else clone octopus, load toolchains and make and test octopus
ENV MODULEPATH=/home/user/spack-environments/lmod/Core:/etc/lmod/modules:/usr/share/lmod/lmod/modulefiles
ENV MPIEXEC=orterun
ENV OMP_NUM_THREADS=1
ENV OCT_TEST_NJOBS=4
ENV OCT_TEST_MPI_NPROCS=2
RUN if [ "${toolchain}" = "global" ]; \
    then exit 0; \
    else \
        git clone https://gitlab.com/octopus-code/octopus /home/user/octopus ; \
    fi
# USER root
#TODO module load the toolchain then make octopus
RUN if [ "${toolchain}" = "global" ]; \
    then exit 0; \
    else \
        . /usr/share/lmod/lmod/init/bash ; module load toolchains/${toolchain} && \
        cd /home/user/octopus && \
        autoreconf -i && \
        mkdir _build && \
        cd _build && \
        echo "sourcing configure with ${MPSD_OCTOPUS_CONFIGURE}" && \
        bash "$MPSD_OCTOPUS_CONFIGURE" --prefix=/home/user/octopus/_build/installed && \
        make -j 16 && \
        make install ; \
    fi

RUN if [ "${toolchain}" = "global" ]; \
    then exit 0; \
    else \ 
        . /usr/share/lmod/lmod/init/bash ; \
        module load toolchains/${toolchain} && \
        cd /home/user/octopus/_build && \
        make check-short | tee /home/user/octopus/_build/check-short.log ; exit 0 ; \
    fi

# we add the exit 0 to make sure the container doesn't fail if the tests fail
# the python script bellow determines from the log if the tests failures can be ignored or not

RUN if [ "${toolchain}" = "global" ]; \
    then exit 0; \
    else \ 
        python /home/user/spack-environments/docker/check_buildlog.py /home/user/octopus/_build/check-short.log ${toolchain}|| /bin/true ; \
    fi
CMD /bin/bash -l
