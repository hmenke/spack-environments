#!/bin/sh
export CC="gcc"
MARCH_FLAG="-march=${GCC_ARCH:-native}"
OPTIMISATION_LEVEL="-O3"
export CFLAGS="$MARCH_FLAG $OPTIMISATION_LEVEL -g -fno-var-tracking-assignments"
export CXX="g++"
export CXXFLAGS="$MARCH_FLAG $OPTIMISATION_LEVEL -g -fno-var-tracking-assignments"
export FC="gfortran"
export FCFLAGS="$MARCH_FLAG $OPTIMISATION_LEVEL -g -fno-var-tracking-assignments -ffree-line-length-none -fallow-argument-mismatch -fallow-invalid-boz"


#HG: ugly hack to include rpath while linking
#    becomes necessary for spack >= 0.19, as it does not set LD_LIBRARY_PATH anymore
export LDFLAGS=`echo ${LIBRARY_PATH:+:$LIBRARY_PATH} | sed -e 's/:/ -Wl,-rpath=/g'`

# HG: help configure to find the ELPA F90 modules and libraries
try_mpsd_elpa_version=`expr match "$MPSD_ELPA_ROOT" '.*/elpa-\([0-9.]\+\)'`
if [ -n "$try_mpsd_elpa_version" ] ; then
    if [ -r $MPSD_ELPA_ROOT/include/elpa_openmp-$try_mpsd_elpa_version/modules/elpa.mod ]; then
        export FCFLAGS_ELPA="-I$MPSD_ELPA_ROOT/include/elpa_openmp-$try_mpsd_elpa_version/modules"
        export LIBS_ELPA="-lelpa_openmp"
    elif [ -r $MPSD_ELPA_ROOT/include/elpa-$try_mpsd_elpa_version/modules/elpa.mod ] ; then
        export FCFLAGS_ELPA="-I$MPSD_ELPA_ROOT/include/elpa-$try_mpsd_elpa_version/modules"
    fi
fi
unset try_mpsd_elpa_version

# HG: always keep options in the order listed by ``configure --help``
../configure \
    --enable-openmp \
    --with-libxc-prefix="$MPSD_LIBXC_ROOT" \
    --with-libvdwxc-prefix="$MPSD_LIBVDWXC_ROOT" \
    --with-blas="-L$MPSD_OPENBLAS_ROOT/lib -lopenblas" \
    --with-gsl-prefix="$MPSD_GSL_ROOT" \
    --with-fftw-prefix="$MPSD_FFTW_ROOT" \
    --with-nfft="$MPSD_NFFT_ROOT" \
    --with-berkeleygw-prefix="$MPSD_BERKELEYGW_ROOT" \
    --with-sparskit="$MPSD_SPARSKIT_ROOT/lib/libskit.a" \
    --with-nlopt-prefix="$MPSD_NLOPT_ROOT" \
    --with-cgal="$MPSD_CGAL_ROOT" \
    --with-boost="$MPSD_BOOST_ROOT" \
    --with-metis-prefix="$MPSD_METIS_ROOT" \
    --with-psolver-prefix="$MPSD_BIGDFT_PSOLVER_ROOT" \
    --with-futile-prefix="$MPSD_BIGDFT_FUTILE_ROOT" \
    --with-atlab-prefix="$MPSD_BIGDFT_ATLAB_ROOT" \
    --with-dftbplus-prefix="$MPSD_DFTBPLUS_ROOT" \
    --with-netcdf-prefix="$MPSD_NETCDF_FORTRAN_ROOT" \
    --with-etsf-io-prefix="$MPSD_ETSF_IO_ROOT" \
    "$@" | tee 00-configure.log 2>&1
echo "-------------------------------------------------------------------------------" >&2
echo "configure output has been saved to 00-configure.log" >&2
if [ "x${GCC_ARCH-}" = x ] ; then
    echo "Microarchitecture optimization: native (set \$GCC_ARCH to override)" >&2
else
    echo "Microarchitecture optimization: $GCC_ARCH (from override \$GCC_ARCH)" >&2
fi
echo "-------------------------------------------------------------------------------" >&2
