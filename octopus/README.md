# Compiling `octopus` using the new spack-generated modules

## Available toolchains

- foss2021a-serial
- foss2021a-mpi
- foss2021a-cuda-mpi
- foss2022a-serial
- foss2022a-mpi
- foss2022a-cuda-mpi

## Load dependencies via modules

```bash
module use /opt_hpc/linux-debian11/23a/sandybridge/lmod/Core # replace sandybridge with your architecture 23a with release
module load toolchains/foss2021a-serial  # or toolchains/foss2021a-mpi
```

Optionally list all loaded modules if desired.

```bash
module list
```

## Configure and compile `octopus`

- Inside octopus: `mkdir _build`
- Copy script `foss-serial-config.sh` (or `foss-mpi-config.sh`) to `_build`
- optionally set ``GCC_ARCH=native`` (or another target microarchitecture to optimize code for)
- inside `_build`: `source foss-serial-config.sh`
- `make`
- (``MPIEXEC="orterun" make check``)

## Missing libraries
- netcdf-fortran
- etsf-io
- dftbplus
- psolver
- futile
- pspio

## Known problems
- `elpa` cannot be used because the configure script does not find the relevant files (inside additional subdirectories).
- `openblas` is not part of the loaded modules (system version is used instead). (Problem: simultanously installing `openblas` and `flexiblas` is not [properly] supported in Spack.)
- 

